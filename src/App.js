import Info from "./components/Info";

function App() {
  return (
    <div>
      <Info firstName="Minh" lastName="Vu Dang" favNumber={26}>Inner HTML</Info>
    </div>
  );
}

export default App;
